<?php
$errors = [];
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (strlen($_POST['firstName']) < 2) {
        $errors[] = 'Eesnimi peab olema vähemalt 2 tähemärki pikk!';
    }
    if (strlen($_POST['lastName']) < 2) {
        $errors[] = 'Perekonnanimi peab olema vähemalt 2 tähemärki pikk!';
    }
    if (empty($_POST['phone1']) && empty($_POST['phone2']) && empty($_POST['phone3'])) {
        $errors[] = 'Vähemalt üks telefoninumber peab olema täidetud!';
    }
    if (empty($errors)) {
        if (isset($_POST['contact'])) {
            updateContact($_POST['firstName'], $_POST['lastName'], @$_POST['phone1'], @$_POST['phone2'], @$_POST['phone3'], $_POST['contact']);
        } else {
            writeToSqlite($_POST['firstName'], $_POST['lastName'], @$_POST['phone1'], @$_POST['phone2'], @$_POST['phone3']);
        }
        header('Location: index.php?command=show_list_page', true, 302);
        exit();
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title Page</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<?php
$contact = null;
if (isset($_GET['edit'])) {
    $sql = "SELECT contacts.id as id, firstName, lastName, GROUP_CONCAT(phones.number, ', ') AS numbers FROM contacts JOIN phones ON contacts.id = phones.contact_id WHERE contacts.id = :contact GROUP BY contacts.id";
    $stmt = $connection->prepare($sql);
    $stmt->bindValue(':contact', $_GET['edit']);
    $stmt->execute();
    if ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $numbers = explode(',', $result['numbers']);
        unset($result['numbers']);
        $n = 1;
        foreach ($numbers as $number) {
            $result['phone'.$n++] = $number;
        }
        $contact = $result;
    }
}
function updateContact($firstname, $lastname, $phone1, $phone2, $phone3, $contact_id) {
    global $connection;
    $sql = "UPDATE contacts SET firstName = :first_name, lastName = :last_name WHERE id = :contact_id";
    $update_stmt = $connection->prepare($sql);
    $update_stmt->bindValue(':first_name', $firstname);
    $update_stmt->bindValue(':last_name', $lastname);
    $update_stmt->bindValue(':contact_id', $contact_id);
    $update_stmt->execute();
    $delete_stmt = $connection->prepare("DELETE FROM phones WHERE contact_id = :contact_id");
    $delete_stmt->bindValue(':contact_id', $contact_id);
    $delete_stmt->execute();
    $phone_sql = "INSERT INTO phones (contact_id, number) VALUES (:contact_id, :number)";
    $phone_stmt = $connection->prepare($phone_sql);
    $phone_stmt->bindParam(':contact_id', $contact_id);
    if (!empty($phone1)) {
        $phone_stmt->bindParam(':number', $phone1);
        $phone_stmt->execute();
    }
    if (!empty($phone2)) {
        $phone_stmt->bindParam(':number', $phone2);
        $phone_stmt->execute();
    }
    if (!empty($phone3)) {
        $phone_stmt->bindParam(':number', $phone3);
        $phone_stmt->execute();
    }
}

function writeToSqlite($firstname, $lastname, $phone1, $phone2, $phone3) {
    global $connection;
    $insert_stmt = $connection->prepare("
        INSERT INTO contacts (firstName, lastName)
        VALUES (:first_name, :last_name)
    ");
    $insert_stmt->bindValue(':first_name', $firstname);
    $insert_stmt->bindValue(':last_name', $lastname);
    $insert_stmt->execute();
    $contact_id = $connection->lastInsertId();
    $phone_sql = "INSERT INTO phones (contact_id, number) VALUES (:contact_id, :number)";
    $phone_stmt = $connection->prepare($phone_sql);
    $phone_stmt->bindParam(':contact_id', $contact_id);
    if (!empty($phone1)) {
        $phone_stmt->bindParam(':number', $phone1);
        $phone_stmt->execute();
    }
    if (!empty($phone2)) {
        $phone_stmt->bindParam(':number', $phone2);
        $phone_stmt->execute();
    }
    if (!empty($phone3)) {
        $phone_stmt->bindParam(':number', $phone3);
        $phone_stmt->execute();
    }
}

function getValue($fieldname) {
    global $contact;
    $mode = isset($_GET['edit']) ? 'edit' : 'add';
    if ($mode == 'edit') {
        if (isset($_POST[$fieldname])) {
            return $_POST[$fieldname];
        } else {
            if (isset($contact[$fieldname])) {
                return $contact[$fieldname];
            }
        }
    } else {
        return isset($_POST[$fieldname]) ? $_POST[$fieldname] : '';
    }
}
?>
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <div class="page-header">
                <h2><?php echo !isset($_GET['edit']) ? 'Uus kontakt' : 'Muuda kontakti' ?> <a class="btn btn-primary btn-sm" href="?command=show_list_page" id="list-page-link">Vaata kontakte</a><a class="btn btn-primary btn-sm" href="?command=show_add_page" id="add-page-link">Lisa uus</a></h2>
            </div>
            <?php
            global $errors;
            if (!empty($errors)) {
                echo '<div class="alert alert-danger" id="error-block">';
                foreach ($errors as $error) {
                    echo $error . '<br>';
                }
                echo '</div>';
            }
            ?>
                <form class="form-horizontal" method="post">
                    <?php
                    if (isset($_GET['edit'])) {
                        echo '<input type="hidden" name="contact" value="'.$_GET['edit'].'"/>';
                    }
                    ?>
                    <div class="form-group">
                        <label for="inputFirstname" class="col-sm-3 control-label">Eesnimi</label>
                            <div class="col-sm-9">
                                <input type="text" value="<?php echo trim(getValue('firstName')) ?>" name="firstName" class="form-control" id="inputFirstname"/>
                            </div>
                    </div>
                    <div class="form-group">
                        <label for="inputLastname" class="col-sm-3 control-label">Perekonnanimi</label>
                        <div class="col-sm-9">
                            <input type="text" value="<?php echo trim(getValue('lastName')) ?>" name="lastName" class="form-control" id="inputLastname"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPhone1" class="col-sm-3 control-label">Telefon 1</label>
                        <div class="col-sm-9">
                            <input name="phone1" value="<?php echo trim(getValue('phone1')) ?>" class="form-control" id="inputPhone1"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPhone2" class="col-sm-3 control-label">Telefon 2</label>
                        <div class="col-sm-9">
                            <input name="phone2" value="<?php echo trim(getValue('phone2')) ?>" class="form-control" id="inputPhone2"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPhone3" class="col-sm-3 control-label">Telefon 3</label>
                        <div class="col-sm-9">
                            <input name="phone3" value="<?php echo trim(getValue('phone3')) ?>" class="form-control" id="inputPhone3"/>
                        </div>
                    </div>
                    <input class="btn btn-primary pull-right" type="submit" value="Salvesta" name="submitButton"/>
                </form>
        </div>
    </div>
</div>
</body>
</html>
