<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title Page</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<?php
function printContacts() {
    $sql = "SELECT contacts.id as id, firstName, lastName, GROUP_CONCAT(phones.number, ', ') AS numbers FROM contacts JOIN phones ON contacts.id = phones.contact_id GROUP BY contacts.id";
    global $connection;
    $stmt = $connection->prepare($sql);
    $stmt->execute();
    $contacts = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($contacts as $contact) {
        echo '<tr>';
        echo '<td><a href="?command=show_add_page&edit='.$contact['id'].'">'.$contact['firstName'].'</a></td>';
        echo '<td>'.$contact['lastName'].'</td>';
        echo '<td>'.$contact['numbers'].'</td>';
        echo '</tr>';
    }
}
?>
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <div class="page-header">
                <h2>Kontaktinimekiri <a class="btn btn-primary btn-sm" href="?command=show_add_page" id="add-page-link">Lisa uus</a><a class="btn btn-primary btn-sm" href="?command=show_list_page" id="list-page-link">Vaata kontakte</a></h2>
            </div>
            <table class="table table-bordered">
                <thead>
                    <th>Eesnimi</th>
                    <th>Perekonnanimi</th>
                    <th>Telefoninumbrid</th>
                </thead>
                <tbody>
                <?php
                printContacts();
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>